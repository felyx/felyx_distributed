# felyx_distributed

## Documentation

This script enable to create MMDBs by launching felyx-extraction and felyx-assemble commands with or without Jobard.

## Installation

### Conda environment

```bash
conda create -n felyx_distributed --file https://gitlab.ifremer.fr/felyx/felyx_distributed/-/raw/master/assets/conda/conda-run-linux-64.lock
conda run -n felyx_distributed pip install --extra-index-url https://gitlab.ifremer.fr/api/v4/projects/1829/packages/pypi/simple felyx_distributed
```

### Docker

```bash
docker pull gitlab-registry.ifremer.fr/felyx/felyx_distributed
```


## Development

### Install environment

- Install conda env

```bash
conda create -n felyx_distributed --file https://gitlab.ifremer.fr/felyx/felyx_distributed/-/raw/master/assets/conda/conda-dev-linux-64.lock
```

- Install all dependencies(runtime and dev)

```bash
poetry install -vv
```

### Pre-commit

- Register pre-commit

```bash
pre-commit install
```

- Run the hooks manually

```bash
pre-commit run --all-files
```

### Check code quality

```bash
flake8 .
```

### Run unit tests

TODO

## Launch application

Example : 

```bash
felyx-mmdb --no-jobard --logfile /tmp/logs/e2e_mmdb.log  --mmdb-id S3A_SL__OPE_NRT__cmems_drifter --extraction-memory 1G --url http://localhost:8000 --felyx-extraction-bin /felyx_processor/felyx-extraction --felyx-assemble-bin /felyx_processor/felyx-assemble --jobard-token <token> --workers 1 --priority 10 --extraction-walltime 00:10:00 --assemble-walltime 00:10:00 --assemble-memory 2G --cluster-ssh localhost --manifest-dir /data/felyx/tests/test_jobard/manifests --output-dir /data/felyx/tests/test_jobard/mmdb_dir --start 2021-09-15 --end 2021-09-16  -c /felyx_processor/tests/resources/test_slstr_a_cmems/s3a_mmdb_local.yaml --child-product-dir /data/felyx/tests/test_jobard/child_products --create-miniprod
```
