"""
"""
from argparse import ArgumentParser
import datetime
import glob
import logging
import os
import re
import sys
import time
from pathlib import Path
from typing import Dict, List, Optional, Tuple

from dateutil import parser as date_parser
from dateutil.rrule import DAILY, rrule
import pandas as pd
from pydantic import BaseModel, HttpUrl

import felyx_processor.utils.configuration as cfg
import felyx_processor.main.matchups as flx_matchups
import felyx_processor.main.miniprod as flx_child
from felyx_processor.utils.logging import setup_logging
from felyx_processor.utils.parameters import LogOptions, log_params, \
    add_log_args
from felyx_processor.utils.arguments import parse_all_args


# Check if jobard client is installed
JOBARD_CLIENT_INSTALLED = True
try:
    from jobard_client.client import JobardClient
    from jobard_client.core.exceptions import JobardError
    from jobard_client.core.jobs.models import JobOrderStat, JobOrderSubmit
    from jobard_client.core.core_types import State, states_closed
except Exception:
    JOBARD_CLIENT_INSTALLED = False

    class JobardClient:
        pass

    class JobOrderStat:
        pass


logger = logging.getLogger('felyx.mmdb')


class ExecutionParams(BaseModel):
    # skip jobard orchestration (run in sequential mode)
    no_jobard: bool = False

    # ignore extraction failures (proceed with assembly anyway)
    ignore_failures: bool = False

    # url of jobard api
    url: Optional[HttpUrl] = 'http://localhost:8000'

    # matchup extraction system parameters (PBS, SWARM, HTCONDOR, LOCAL)
    connection: Optional[str] = 'datarmor'
    split: Optional[str] = '/1'
    workers: Optional[int] = 10
    priority: Optional[int] = 10
    job_extra: Optional[List[str]] = []

    # can be overriden in MMDB configuration file (per dataset)
    extraction_walltime: Optional[str] = '02:00:00'
    extraction_memory: Optional[str] = '2G'

    assemble_walltime: Optional[str] = '02:00:00'
    assemble_memory: Optional[str] = '10G'

    # path to felyx-extraction binary
    felyx_extraction_bin: Optional[str] = 'felyx-extraction'
    # path to felyx-extraction binary
    felyx_assemble_bin: Optional[str] = 'felyx-assemble'


class MMDBParams(BaseModel):
    """command arguments"""
    configuration: Path
    child_extraction: Optional[flx_child.MiniprodsParams] = None
    assembling: Optional[flx_matchups.MatchupsParams] = None
    execution: ExecutionParams = ExecutionParams()

    # identifier of MMDB to process, as defined in config file
    mmdb_id: List[str]

    # skip the extraction step, only runs the assembly.
    skip_matchup_search: bool = False
    # skip the assembly step, only runs the matchup search and child product
    # extraction (if child_product_extraction is set).
    skip_matchup_assembly: bool = False

    # resume a previously interrupted processing
    resume: bool = False

    # Jobard token
    jobard_token: Optional[str] = None

    # Jobard Docker image ID for Swarm and K8S clusters
    jobard_image_id: Optional[int] = None

    log: LogOptions = LogOptions()


def add_jobard_extras(jobard_args) -> Dict:
    """
    Compute the jobard extras dictionary.
    """
    job_extra = {'n_workers': jobard_args.workers}
    if 'job_extra' in jobard_args and jobard_args.job_extra is not None:
        job_extra.update({
            _.split('=')[0]: _.split('=')[1]
            for _ in jobard_args.job_extra
        })
    return job_extra

def args_as_dict(args, felyx_sys_config):
    return dict(
        no_jobard=args.no_jobard,
        ignore_failures=args.ignore_failures,
        url=args.url,
        connection=args.connection,
        split=args.split,
        workers=args.workers,
        priority=args.priority,
        job_extra=args.job_extra,
        extraction_walltime=args.extraction_walltime,
        extraction_memory=args.extraction_memory,
        assemble_walltime=args.assemble_walltime,
        assemble_memory=args.assemble_memory,
        felyx_extraction_bin=args.felyx_extraction_bin,
        felyx_assemble_bin=args.felyx_assemble_bin)


def add_execution_args(parser: ArgumentParser) -> ArgumentParser:
    parser.add_argument(
        '--no-jobard', action='store_true',
        help='skip usage of jobard orchestrator (runs the MDB in sequential '
             'mode)')
    parser.add_argument(
        '--ignore-failures', action='store_true',
        help='proceed with the assembly anyway, even if the matchup '
             'extraction step failed for some input files. By default the '
             'MMDB assembly step is cancelled in case of any failure.')
    # processing optional arguments
    parser.add_argument(
        '--split', type=str,
        help='split the list of input files in several jobarrays, e.g. "/10" '
             'to create 10 jobarrays or "10" to create N(files)/10 jobarrays')
    parser.add_argument(
        '--workers', type=str,
        help='number of workers used to process each jobarray')
    parser.add_argument(
        '--priority', type=int, help='processing priority')
    parser.add_argument(
        '--cluster-ssh', type=str, dest='connection',
        help='ssh connection point to the cluster')
    parser.add_argument(
        '--job-extra', type=str, dest='job_extra', nargs='+',
        help='jobard extra parameters'
             '--job-extra mykey:value')
    parser.add_argument(
        '-u', '--url', type=str,
        help='URL of jobard scheduler service (if different from the one '
             'provided in system configuration)')
    parser.add_argument(
        '--extraction-memory', type=str,
        help=str(
            'maximum memory for felyx-extraction (can be overriden per '
            'dataset in MMDB configuration file). Default is `2G`.'
        )
    )
    parser.add_argument(
        '--extraction-walltime', type=str,
        help=str(
            'maximum walltime for felyx-extraction (can be overriden per '
            'dataset in MMDB configuration file). Default is `02:00:00`.'
        )
    )
    parser.add_argument(
        '--assemble-memory', type=str,
        help=str(
            'maximum memory for felyx-assemble (can be overriden per '
            'dataset in MMDB configuration file). Default is `2G`.'
        )
    )
    parser.add_argument(
        '--assemble-walltime', type=str,
        help=str(
            'maximum walltime for felyx-assemble (can be overriden per '
            'dataset in MMDB configuration file). Default is `02:00:00`.'
        )
    )
    parser.add_argument(
        '--felyx-extraction-bin', type=str,
        help=str(
            'Path to `felyx-extraction` command if running in a non '
            'conda/docker environment'
        )
    )
    parser.add_argument(
        '--felyx-assemble-bin', type=str,
        help=str(
            'Path to `felyx-assemble` command if running in a non '
            'conda/docker environment'
        )
    )

    return parser


def parse_args(
        cli_args: List[str]
) -> Tuple[cfg.FelyxSystemConfig, cfg.FelyxProcessorConfig, MMDBParams]:
    """"""
    parser = ArgumentParser()
    parser.add_argument(
        '-c', '--configuration-file', type=Path,
        help=str(
            'A configuration file with the settings of the match-up dataset '
            'to assemble'
            )
        )
    parser.add_argument(
        '--mmdb-id', type=str, dest='mmdb_id', nargs='+',
        help=str(
            'the list of identifiers, within the MMDB configuration file of '
            'the matchup dataset to generate'
        )
    )
    parser.add_argument(
        '--child-product-dir', type=str,
        help='Path of the directory that will be used as root '
             'when building the path of the files containing the extractions')
    parser.add_argument(
        '--manifest-dir', type=str,
        help='Path of the directory that will be used as root to store '
             'manifest.\nIf set to "-" (default), the content of the manifest '
             'file is printed on stdout')

    # Jobard token option
    parser.add_argument(
        '--jobard-token', type=str, dest='jobard_token',
        help='Jobard API access token if using Jobard')

    # Jobard Docker image ID (for Swarm and K8S clusters).
    parser.add_argument(
        '--jobard-image-id', type=int, dest='jobard_image_id', default=None,
        help='Jobard Docker image ID if using Jobard.')


    parser.add_argument(
        '--skip-matchup-search', action='store_true',
        help='skip the felyx-extraction step (only runs assembly) which finds '
             'the matchups (possibly extracting them as child products if '
             '--child-product-extraction is set. This step must then have been '
             'run before.')
    parser.add_argument(
        '--skip-matchup-assembly', action='store_true',
        help='skip the felyx-assemble step, only runs the matchup search ('
             'possibly extracting them as child products if '
             '--child-product-extraction is set. This step must then have been '
             'run before.')

    # TODO use breakpoint argument
    parser.add_argument(
        '--resume', action='store_true',
        help='resume a previously launched processing (in case of '
             'interruption)')

    # child extraction, assembling and distributed execution arguments
    parser = flx_child.add_child_extraction_args(parser)
    parser = flx_matchups.add_assembling_args(parser)
    parser = add_execution_args(parser)

    # logs and verbosity arguments
    parser = add_log_args(parser)

    args = parse_all_args(parser, cli_args)

    felyx_processor_config = cfg.load_felyx_processor_config(
        configuration_file=args.configuration_file)
    felyx_sys_config = cfg.load_felyx_sys_config()

    # dummy args (filled in later)
    vars(args)['dataset_id'] = ""
    vars(args)['inputs'] = ""
    vars(args)['from_list'] = None
    vars(args)['matchup_dataset'] = ""

    params = MMDBParams(**dict(
        configuration=args.configuration_file,
        mmdb_id=args.mmdb_id,
        child_extraction=flx_child.args_as_dict(args, felyx_sys_config),
        assembling=flx_matchups.args_as_dict(args, felyx_sys_config),
        execution=args_as_dict(args, felyx_sys_config),
        skip_matchup_search=args.skip_matchup_search,
        skip_matchup_assembly=args.skip_matchup_assembly,
        resume=args.resume,
        log=log_params(args),
        jobard_token=args.jobard_token,
        jobard_image_id=args.jobard_image_id,
    ))

    return felyx_sys_config, felyx_processor_config, params


def exit_success():
    """"""
    logger.info('Done (success)')
    sys.exit(0)


def exit_failure():
    """"""
    logger.error('Done (failure)')
    sys.exit(1)


def wait_for_job_order(client: JobardClient,
                       job_order_id: int,
                       token: str) -> JobOrderStat:
    job_order_stat: JobOrderStat = client.stat(job_order_id=job_order_id, token=token)
    delay: int = 0
    timeout: int = -1
    increment: int = 60

    while job_order_stat.state not in states_closed:
        print(job_order_stat)
        delay += increment
        time.sleep(increment)
        if delay >= timeout > -1:
            raise TimeoutError()
        job_order_stat = client.stat(job_order_id=job_order_id, token=token)

    return job_order_stat


def wait_for_job_orders(
        client: JobardClient,
        job_order_ids: Dict[str, int],
        token: str
) -> Dict[str, JobOrderStat]:
    """Wait for a sequence of job orders to complete"""
    job_order_stats: Dict[str, JobOrderStat] = {}
    delay: int = 3600
    timeout: int = -1
    increment: int = 30

    # loop while not all jobs closed (failed, succeeded)
    while True:
        # get status of all jobs
        job_status = {}
        job_progress = {}
        for dataset_id, job_order_id in job_order_ids.items():
            response = client.stat(job_order_id=job_order_id, token=token)
            job_status[dataset_id] = response.state
            job_progress[dataset_id] = response.progress.job_states

        # print progress at the end of each loop
        report = ' '.join([
            f'{dataset_id}: {progress}'
            for dataset_id, progress in job_progress.items()])
        print(report)

        # break if all jobs completed
        if all([status in states_closed for status in job_status.values()]):
            break

        time.sleep(increment)

    # status of jobs
    job_order_stats.update({
            dst: client.stat(job_order_id=job_id, token=token)
            for dst, job_id in job_order_ids.items()
        })

    return job_order_stats

    # while True:
    #     job_order_stats.update({
    #         dst: client.stat(job_order_id=job_id)
    #         for dst, job_id in job_order_ids.items()
    #     })
    #     progress = '\r' + " ".join([
    #         f'{dst}: ({job_order_stat.progress})'
    #         for dst, job_order_stat in job_order_stats.items()
    #     ])
    #     print(progress)
    #     print(list(job_order_stats.values()),
    #           all([status.state in states_closed
    #                for status in job_order_stats.values()])
    #           )
    #
    #     if all([status.state in states_closed
    #             for status in job_order_stats.values()]):
    #
    #         logger.info(progress)
    #         return job_order_stats
    #
    #     delay += increment
    #     time.sleep(increment)
    #     if delay >= timeout > -1:
    #         logger.info(progress)
    #         logger.info('felyx-extraction is taking too much time to '
    #                     'complete. You may want to check if there is '
    #                     'something wrong on the processing workers or extend '
    #                     'this time-out.')
    #         raise TimeoutError()


def collect_input_files(dataset_id, config, start, end):
    filenaming = config.EODatasets[dataset_id].filenaming
    # Check if filenaming is defined
    if not filenaming:
        raise ValueError(
            f'Missing path for dataset {dataset_id}. Check filenaming field in the configuration file')

    # discard all time markers < day
    # @TODO improve for files not stored by day
    filenaming = re.sub('\*+', '*', re.sub(r'%([HIMSfcx])', '*', filenaming))

    files = []

    # @TODO improve for files not stored by day
    # add last file of previous day (to manage overlap over 00:00 UTC)
    prev_files = glob.glob(
        (start - datetime.timedelta(days=1)).strftime(filenaming))
    if len(prev_files) > 0:
        files.append(prev_files[-1])

    # get files over requested days
    for dt in rrule(
            DAILY, dtstart=start, until=end - datetime.timedelta(days=1)):
        logger.debug(f'search path for {dataset_id}: {dt.strftime(filenaming)}')
        files.extend(
            glob.glob(dt.strftime(filenaming))
        )

    files = list(set(files))
    files.sort()

    return files


def run_extraction(client, processing_args, config):

    joborders = {}
    file_count = {}
    files = {}

    # skip datasets not part of the produced MMDBs or siblings of a parent
    # dataset
    datasets_to_process = []
    logger.info('Determining the list of datasets to extract match-ups from')
    for mmdb_id in processing_args.mmdb_id:
        logger.info(f'Checking MMDB {mmdb_id}')
        mmdb_settings = config.MMDatasets[mmdb_id]
        for dataset_id in mmdb_settings.eo_datasets:
            # skip dataset if it is a sibling of the reference dataset
            dst_config = mmdb_settings.eo_datasets[dataset_id]
            if (not processing_args.child_extraction.options.create_miniprod
                    and config.EODatasets[dataset_id].sibling is not None):
                logger.info(
                    f'Skipping extraction of sibling product {dataset_id}')
                continue

            if dataset_id not in datasets_to_process:
                datasets_to_process.append(dataset_id)

    # collect input files for dataset
    for dataset_id in datasets_to_process:

        collfiles = collect_input_files(
            dataset_id, config, processing_args.assembling.start,
            processing_args.assembling.end)
        if len(collfiles) == 0:
            logger.warning(f'No input files found for {dataset_id}')

        files[dataset_id] = collfiles
        file_count[dataset_id] = len(files[dataset_id])

    # process
    for dataset_id in datasets_to_process:

        if file_count[dataset_id] == 0:
            logging.warning(f'skipping matchup extraction for {dataset_id}')
            continue

        # dump list in a file
        # eo_file_list = processing_args.manifest_dir / \
        #     f'eo_{dataset_id}_{datetime.datetime.now().isoformat()}.list'
        # with open(eo_file_list, 'w') as fl:
        #     for f in files:
        #         fl.write(str(f) + '\n')

        extraction_args = [
            '-c', str(processing_args.configuration),
            '--dataset-id', dataset_id,
            '--manifest-dir', str(
                processing_args.child_extraction.options.manifest_dir),
            '--child-product-dir', str(
                processing_args.child_extraction.options.child_product_dir),
        ]

        # extract child products in addition to manifests
        if processing_args.child_extraction.options.create_miniprod:
            extraction_args.append('--create-miniprod')

        extraction_args.append('--inputs')

        logger.info('\n')
        logger.info('###### Processing matchup extraction ######')
        if processing_args.execution.no_jobard:
            # add input files to arguments
            extraction_args.extend([_ for _ in files[dataset_id]])

            # runs in sequential mode without jobard
            try:
                flx_child.felyx_miniprod(extraction_args)
            except SystemExit:
                print(sys.exc_info()[1].code == 0)

        else:
            try:
                jobard_args = processing_args.execution
                if dst_config.distributed_processing is not None:
                    memory = dst_config.distributed_processing.get(
                        'memory', jobard_args.extraction_memory
                    )
                    walltime = dst_config.distributed_processing.get(
                        'walltime', jobard_args.extraction_walltime
                    )
                else:
                    memory = jobard_args.extraction_memory
                    walltime = jobard_args.extraction_walltime

                job_extra = add_jobard_extras(jobard_args)

                # submit a job order and retrieve job order id
                logger.info(
                    f'firing job order for {dataset_id} with '
                    f'{file_count[dataset_id]} files to process')
                job_order_id: int = client.submit(
                    JobOrderSubmit(
                        command=[
                            jobard_args.felyx_extraction_bin,
                            *extraction_args,
                        ],
                        arguments=[[_] for _ in files[dataset_id]],
                        cores=1,
                        split=jobard_args.split,
                        memory=memory,
                        walltime=walltime,
                        connection=jobard_args.connection,
                        priority=jobard_args.priority,
                        job_extra=[f'{key}={value}' for key, value in job_extra.items()],
                        image_id= processing_args.jobard_image_id
                    ),
                    token=handle_token(token_option=processing_args.jobard_token)
                )
                joborders[dataset_id] = job_order_id
                logger.info(f'job id for dataset {dataset_id}: {job_order_id}')

            except JobardError as error:
                logger.error(f'error firing joborder for {dataset_id}')
                logger.error(error)
                exit_failure()

    if processing_args.execution.no_jobard:
        return

    failures = wait_for_completion(
        client, joborders, 'felyx-extraction', expected_count=None,
        token=handle_token(token_option=processing_args.jobard_token))

    return failures


def wait_for_completion(client, joborders, job_label, token, expected_count=None):
    """Loop until the completion of all jobs and returns the failures"""
    if len(joborders) == 0:
        logger.error('No processing jobs were triggered. Check your arguments')
        exit_failure()

    # loop while processing is not completed
    try:
        job_order_stats = wait_for_job_orders(client, joborders, token)
        logger.info(f'processing summary for {job_label}')
        logger.info('---------------------------------------')
        for dst, stats in job_order_stats.items():
            if expected_count is None:
                logger.info(f'{dst}: {stats.progress.job_states}')
            else:
                logger.info(
                    f'{dst}: {stats.progress.job_states} over '
                    f'{expected_count[dst]}')

    except TimeoutError:
        print('job order execution timeout')

    # list arguments for failed execution
    failures = {}
    for dst, job_order_id in joborders.items():
        failed_inputs = list(client.all_arguments(
            job_order_id=job_order_id, states=[State.FAILED], token=token))
        if len(failed_inputs) > 0:
            failures[dst] = failed_inputs
            logger.warning('--------------------------------------')
            logger.warning(f'The processing failed for the following files of '
                           f'{dst}')
            for f in failed_inputs:
                logger.warning(f[0])
            logger.warning('--------------------------------------')
        else:
            continue

        logger.warning('check out the following logs:')
        for job in client.all_jobs(job_order_id=job_order_id,
                                   states=[State.FAILED],
                                   token=token):
            logger.warning(job.log_path_stdout)
            logger.warning(job.log_path_stderr)
        logger.warning('--------------------------------------')

    logger.warning('check out the following successful logs:')
    for job in client.all_jobs(job_order_id=job_order_id,
                               states=[State.SUCCEEDED],
                               token=token):
        logger.info(job.log_path_stdout)
    logger.warning('--------------------------------------')

    return failures


def run_assembling(client, processing_args, config):
    joborders = {}

    assemble_args = [
        '--configuration', str(processing_args.configuration),
        '-o', str(processing_args.assembling.output_dir),
        '--manifest-dir', str(processing_args.assembling.manifest_dir),
        '--from-manifests',
        '--start', processing_args.assembling.start.isoformat(),
        '--end', processing_args.assembling.end.isoformat(),
    ]

    if not processing_args.child_extraction.options.create_miniprod:
        assemble_args.extend([
            '--extract-from-source'
            ])
    else:
        assemble_args.extend([
            '--child-product-dir',
            str(processing_args.child_extraction.options.child_product_dir)
            ])

    assemble_args.append('--matchup-dataset')

    logger.info('\n')
    logger.info('###### Processing matchup assembling ######')

    # @TODO parallelise also by days
    for mmdb_id in processing_args.mmdb_id:
        if processing_args.execution.no_jobard:
            assemble_args.append(mmdb_id)

            # runs in sequential mode without jobard
            flx_matchups.assemble_matchups(assemble_args)

        else:
            try:
                jobard_args = processing_args.execution
                memory = jobard_args.assemble_memory
                walltime = jobard_args.assemble_walltime
                job_extra = add_jobard_extras(jobard_args)

                # break down extraction period into regular intervals
                mdb_config = config.MMDatasets[mmdb_id]
                frequency = f'{mdb_config.output.frequency}'
                start = pd.date_range(
                    end=processing_args.assembling.start, periods=1, freq=frequency)[0]
                stop = pd.date_range(
                    start=processing_args.assembling.end, periods=1, freq=frequency)[0]
                intervals = pd.date_range(
                    start=min(start, stop), end=max(stop, start),
                    freq=frequency)

                for i in range(len(intervals) - 1):
                    assemble_args[assemble_args.index('--start')+1] = \
                        intervals[i].strftime('%Y-%m-%dT%H:%M:%S')
                    assemble_args[assemble_args.index('--end')+1] = \
                        intervals[i+1].strftime('%Y-%m-%dT%H:%M:%S')
                    job_order_id: int = client.submit(
                        JobOrderSubmit(
                            command=[
                                processing_args.execution.felyx_assemble_bin,
                                *assemble_args,
                            ],
                            arguments=[[mmdb_id]],
                            cores=1,
                            split=processing_args.execution.split,
                            memory=memory,
                            walltime=walltime,
                            connection=processing_args.execution.connection,
                            priority=processing_args.execution.priority,
                            job_extra=[
                                f'{key}={value}' for key, value in
                                job_extra.items()],
                            image_id=processing_args.jobard_image_id
                        ),
                    token=handle_token(token_option=processing_args.jobard_token))

                    label = f'{mmdb_id}_' \
                        f'{intervals[i].strftime("%Y-%m-%dT%H:%M:%S")}_' \
                        f'{intervals[i+1].strftime("%Y-%m-%dT%H:%M:%S")}'
                    joborders[label] = job_order_id
                    print(label, job_order_id, assemble_args)

            except JobardError as error:
                logger.error(error, exc_info=sys.exc_info())
                exit_failure()

            failures = wait_for_completion(
                client, joborders, 'felyx-assemble', expected_count=None,
                token=handle_token(token_option=processing_args.jobard_token))


def handle_token(token_option: str) -> str:
    """Handle token either from environment variable or command option, the latter being priority.

    Args:
        token_option: The token passed in option.

    Returns:
        str: Either option or environment variable token
    """
    token = os.getenv('JOBARD_TOKEN')
    if token_option:
        token = token_option
    return token

def felyx_mmdb(cli_args: List[str] = None):
    """"""
    # get command line args
    felyx_sys_config, felyx_processor_config, args = parse_args(cli_args)

    # Setup logging
    if not setup_logging(logger, args.log):
        logger.error('Done (failure)')
        sys.exit(1)

    # If Jobard client is not installed, force no_jobard option to True
    if JOBARD_CLIENT_INSTALLED is False:
        if args.execution.no_jobard is False:
            logger.info(
                f'Jobard client is not installed, the process will be run in '
                f'sequential mode')
        args.execution.no_jobard = True

    try:
        if args.execution.no_jobard:
            client = None
        else:
            # Retrieve the Jobard token

            logger.info(f'connecting to jobard API: {args.execution.url}')
            client = JobardClient(str(args.execution.url))

            logger.debug('connected')

        # search matchups
        if not args.skip_matchup_search:
            logger.info('extracting match-ups....')
            failures = run_extraction(client, args, felyx_processor_config)

            if not args.execution.ignore_failures and failures is not None:
                for dst, errors in failures.items():
                    if len(errors) != 0:
                        logger.error(f'At lease one extraction failed for '
                                     f'{dst}. Exiting.')
                        exit_failure()
        else:
            logger.info('Skipping the matchup search and extraction ('
                        '--skip-matchup-search)')

        # build MDB files
        logger.info('assembling matchups.....')
        if not args.skip_matchup_assembly:
            run_assembling(client, args, felyx_processor_config)
        else:
            logger.info('Skipping the matchup assembling step ('
                        '--skip-matchup-assembly)')

    except Exception:
        # Error sink
        logger.error('An error occurred and could not be handled by felyx',
                     exc_info=sys.exc_info())
        exit_failure()

    exit_success()
