from pathlib import Path

import pytest

from felyx_distributed.main.run_mmdb import felyx_mmdb

TEST_DIR = Path(__file__).parent.resolve()

CONFIG_FILE = Path(
    'tests/resources/test_slstr_a_cmems/s3a_mmdb.yaml')

TEST_DATASET = 'S3A_SL_2_WST__OPE_NRT'


@pytest.fixture(scope='module')
def output_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')


@pytest.fixture
def mmdb_args(output_dir):

    (Path(output_dir) / Path('manifests')).mkdir(parents=True, exist_ok=True)
    (Path(output_dir) / Path('mmdb_dir')).mkdir(parents=True, exist_ok=True)

    print('MMDB Dir ', (Path(output_dir) / Path('mmdb_dir')))

    yield ['--mmdb-id', 'S3A_SL__OPE_NRT__cmems_drifter',
           '--no-jobard',
           '--manifest-dir', f'{output_dir}/manifests/',
           '--output-dir', f'{output_dir}/mmdb_dir/',
           '--start', '2021-09-15',
           '--end', '2021-09-16',
           '--logfile', f'{output_dir}/e2e_mmdb.log',
           '-v',
           '-c', str(CONFIG_FILE),
           ]


def test_mmdb_single_swath(output_dir, mmdb_args):

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_mmdb(mmdb_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    mmdb_file = (Path(output_dir) /
        'mmdb_dir/2021/20210915000000_S3A_SL__OPE_NRT__cmems_drifter.nc')

    print(mmdb_file)

    assert mmdb_file.exists()


def test_mmdb_single_swath_with_extraction(output_dir, mmdb_args):

    # remove previous MMDB file
    mmdb_file = (Path(output_dir) /
        'mmdb_dir/2021/20210915000000_S3A_SL__OPE_NRT__cmems_drifter.nc')
    if mmdb_file.exists():
        mmdb_file.unlink()

    child_product_dir = Path(output_dir) / 'child_products'
    child_product_dir.mkdir()

    # local child product extraction
    mmdb_args.insert(0, '--child-product-dir')
    mmdb_args.insert(1, str(child_product_dir))

    # no child product extraction
    mmdb_args.insert(0, '--create-miniprod')
    print(mmdb_args)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_mmdb(mmdb_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    mmdb_file = (Path(output_dir) /
        'mmdb_dir/2021/20210915000000_S3A_SL__OPE_NRT__cmems_drifter.nc')

    assert mmdb_file.exists()


def test_mmdb_single_swath_assembly(output_dir, mmdb_args):

    # remove previous MMDB file
    mmdb_file = (Path(output_dir) /
        'mmdb_dir/2021/20210915000000_S3A_SL__OPE_NRT__cmems_drifter.nc')
    if mmdb_file.exists():
        mmdb_file.unlink()

    mmdb_args.insert(0, '--skip-matchup-assembly')

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_mmdb(mmdb_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    print(output_dir)

    assert not mmdb_file.exists()


def test_mmdb_single_swath_dummy_jobard_args(output_dir, mmdb_args):

    # remove previous MMDB file
    mmdb_file = (Path(output_dir) /
        'mmdb_dir/2021/20210915000000_S3A_SL__OPE_NRT__cmems_drifter.nc')
    if mmdb_file.exists():
        mmdb_file.unlink()

    # no child product extraction
    mmdb_args.extend([
        '--job-extra',
        'image:gitlab-registry.ifremer.fr/felyx/felyx_processor:latest',
        'myket:value'])
    print(mmdb_args)

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        felyx_mmdb(mmdb_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    mmdb_file = (Path(output_dir) /
        'mmdb_dir/2021/20210915000000_S3A_SL__OPE_NRT__cmems_drifter.nc')

    assert mmdb_file.exists()
